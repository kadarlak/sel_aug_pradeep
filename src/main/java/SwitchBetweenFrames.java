import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class SwitchBetweenFrames {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver d=new ChromeDriver();
		d.navigate().to("https://jqueryui.com/droppable/");
		
	
	//	WebElement frame = d.findElementByXPath("0");
				d.switchTo().frame(0);
		WebElement drag = d.findElementById("draggable");
		WebElement drop = d.findElementById("droppable");
		Actions act=new Actions(d);
		act.dragAndDrop(drag, drop).perform();
		d.switchTo().parentFrame();//to go to immeadiate parent frame
		d.switchTo().defaultContent();//to come to main webpage source
		

	}

}
