package testcase;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.internal.LocatingElementListHandler;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class Tc_006MergeLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName="Tc_006MergeLead";
		testCaseDesc="";
		author="pradeep";
		category="smoke";

	}
	@Test

	public void mergeLead() {
		WebElement leads = locateElement("xpath", "//a[text()='Leads']");
		click(leads);
		WebElement mergeLeads = locateElement("xpath", "//a[text()='Merge Leads']");
		click(mergeLeads);
		WebElement fromButton = locateElement("xpath", "//td[@class='titleCell']//following::img");
		click(fromButton);

		switchToWindow(1);
		WebElement sendId = locateElement("xpath", "//input[@name='id']");
		type(sendId, "103");
		WebElement findLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeads);
		WebElement firstId = locateElement("xpath", " //a[@class='linktext']");
		click(firstId);
		switchToWindow(0);
		WebElement toButton = locateElement("xpath","//td[@class='titleCell']//following::img[2]");
		click(toButton);
		switchToWindow(1);
		WebElement sendIdTo = locateElement("name", "id");
		type(sendIdTo, "102");
		WebElement findLeadsTo = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadsTo);
		WebElement firstIdTo = locateElement("xpath", "//a[@class='linktext']");
		click(firstIdTo);
		switchToWindow(0);
		WebElement merge = locateElement("xpath", "//a[text()='Merge']");
		click(merge);
		acceptAlert();
		WebElement findLeadsagain = locateElement("xpath","//a[text()='Find Leads']");
		click(findLeadsagain);



	}

}
