package testcase;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class HtmlReportMergeLead {

	public static void main(String[] args) throws IOException {
		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/mergeLead.html");
		html.setAppendExisting(true);
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);
		
		
		ExtentTest test = extent.createTest("Tc_006MergeLead", "This will merge the given two Lead records");
		test.assignCategory("smoke");
		test.assignAuthor("pradeep");
		
		test.pass("broswer has launched succesfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img.png").build());
		extent.flush();
		

	}

}
