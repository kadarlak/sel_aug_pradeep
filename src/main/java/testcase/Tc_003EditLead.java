package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethodsOld.SeMethods;

public class Tc_003EditLead extends ProjectMethods{
	
	@BeforeTest
	
	public void setData() {
	testCaseName="Tc_003EditLead";
	testCaseDesc="editng the lead record";
	author="pradeep";
	category="smoke";
	}
	@Test(enabled=false)
	public void editLead() throws InterruptedException {
		/*startApp("chrome", "http://leaftaps.com/opentaps/control/main");
		WebElement user = locateElement("id", "username");
		type(user, "DemoSalesManager");
		WebElement pass = locateElement("id","password");
		type(pass, "crmsfa");
		WebElement login = locateElement("class","decorativeSubmit");
		
		click(login);
		
		WebElement crmLink = locateElement("linktext","CRM/SFA");
		click(crmLink);*/
		WebElement leads = locateElement("xpath", "//a[text()='Leads']");
		click(leads);
		WebElement findLeads = locateElement("xpath", "//a[text()='Find Leads']");
		click(findLeads);
		Thread.sleep(5000);
		WebElement firstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(firstName,"Pradeep");
		WebElement findLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadsButton);
		Thread.sleep(5000);
		WebElement firstRecordId = locateElement("xpath", "(//a[@class='linktext'])[4]");
		
		click(firstRecordId);
		
		verifyTitle("Find Leads | opentaps CRM");
		
		
		WebElement edit = locateElement("xpath", "(//a[@class='subMenuButton'])[3]");
		click(edit);
		WebElement companyName = locateElement("updateLeadForm_companyName");
		companyName.clear();
		type(companyName, "TCS");
		WebElement update = locateElement("name", "submitButton");
		click(update);
		WebElement newCompanyName = locateElement("viewLead_companyName_sp");
		verifyExactText(newCompanyName, "TCS");
		closeBrowser();
		
		
	}

}
