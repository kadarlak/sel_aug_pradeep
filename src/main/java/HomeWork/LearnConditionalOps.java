package HomeWork;

import java.util.Scanner;

import org.apache.poi.util.SystemOutLogger;

public class LearnConditionalOps {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("enter the first value:\n");
		Scanner s= new Scanner(System.in);
		int a =s.nextInt();
		System.out.println("enter the second value:\n");
		int b= s.nextInt();
		
		if(a==b) 
			System.out.println("both a and b are equal");
		else if(a>b)
		System.out.println("a is greater than b");
		else if(a<b)
			System.out.println("a is less than b");
		else if(a<=b)
			System.out.println("a is less than or equals to b");
		else if(a>=b)
			System.out.println("a is greater than or equals to b");
		else if(a!=b)
			System.out.println("a not equals to b");
		

	}

}
