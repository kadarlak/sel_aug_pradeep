package HomeWork;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class Mergeleads {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver d=new ChromeDriver();
		d.manage().window().maximize();
		d.navigate().to("http://leaftaps.com/opentaps/control/main");
		d.findElementById("username").sendKeys("DemoSalesManager");
		d.findElementById("password").sendKeys("crmsfa");
		d.findElementByXPath("//input[@class='decorativeSubmit']").click();
		d.findElementByXPath("//div[@id='label']/a").click();
		d.findElementByXPath("//a[text()='Leads']").click();
		d.findElementByLinkText("Merge Leads").click();
		d.findElementByXPath("//td[@class='titleCell']//following::img").click();
		Set<String> winIds = d.getWindowHandles();
		List<String> l=new ArrayList<String>(winIds);
		d.switchTo().window(l.get(1));
		System.out.println(d.getTitle());
		d.findElementByName("id").sendKeys("10232");
		d.findElementByClassName("x-btn-text").click();
		Thread.sleep(5000);
		d.findElementByXPath("//a[@class='linktext']").click();
		d.switchTo().window(l.get(0));
		d.findElementByXPath("//table[@id='widget_ComboBox_partyIdTo']//following::a").click();
		winIds = d.getWindowHandles();
		l.addAll(winIds);
		System.out.println(winIds);
		d.switchTo().window(l.get(3));
		d.findElementByName("id").sendKeys("10234");
		d.findElementByClassName("x-btn-text").click();
		Thread.sleep(5000);
		d.findElementByXPath("//a[@class='linktext']").click();
		d.switchTo().window(l.get(0));
		d.findElementByClassName("buttonDangerous").click();
		
		Alert alert = d.switchTo().alert();
		
		alert.accept();
		d.findElementByLinkText("Find Leads").click();
		d.findElementByName("id").sendKeys("10185");
		d.findElementByClassName("x-btn-text").click();
		
		d.close();
		
		
		

	}

}
