package HomeWork;

import java.util.Scanner;

public class LearnTableForm {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		System.out.println("enter any number that you need a table for\n");
		Scanner s=new Scanner(System.in);
		int a=s.nextInt();
		int i=1;
		
		while(i<=20) {
		System.out.println(i +"* "+a+ "= "+i*a);
		i++;
		}
	
		//using for loop
		System.out.println("-----------------------------------------------------");
		
		for(i=1;i<=20;i++) {
		
			System.out.println(i +"* "+a+ "= "+i*a);
		}

	}

}
