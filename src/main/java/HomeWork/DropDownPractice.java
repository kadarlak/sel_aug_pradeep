package HomeWork;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class DropDownPractice {

	@Test
		public void DropDown() {
		
		
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			ChromeDriver d=new ChromeDriver();
			d.manage().window().maximize();
			d.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				d.navigate().to("http://leaftaps.com/opentaps/control/main");
				d.manage().window().maximize();
				d.findElement(By.id("username")).sendKeys("DemoSalesManager");
				d.findElement(By.id("password")).sendKeys("crmsfa");
				d.findElement(By.className("decorativeSubmit")).click();
				d.findElementByLinkText("CRM/SFA").click();
				d.findElement(By.linkText("Create Lead")).click();
				WebElement source = d.findElement(By.id("createLeadForm_dataSourceId"));
				Select s=new Select(source);
				//s.selectByVisibleText("Direct Mail");
				List<WebElement> dropElements = source.findElements(By.tagName("option"));
				for(int i=0;i<dropElements.size();i++) {
					String text = dropElements.get(i).getText();
					System.out.println(text);
					
					if(text.equals("Conference")) {
						s.selectByVisibleText(text);
					}
				}
				
				WebElement marketCamp = d.findElement(By.id("createLeadForm_marketingCampaignId"));
				Select droplist =new Select(marketCamp);
				List<WebElement> valueList = marketCamp.findElements(By.tagName("option"));
				
				droplist.selectByIndex(valueList.size()-1);
			} catch (Exception e) {
				System.err.println("new Run time exception has occurred");
			}
			finally {
				d.close();
			}
			
			
				
				
				}
			}
			
			
	
	

