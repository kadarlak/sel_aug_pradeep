package HomeWork;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TablePractice {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver d=new ChromeDriver();
		d.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		d.manage().window().maximize();
		d.get("http://leaftaps.com/opentaps/control/main");
		d.findElement(By.id("username")).clear();
		d.findElement(By.id("username")).sendKeys("DemoSalesManager");
		d.findElementById("password").clear();
		d.findElementById("password").sendKeys("crmsfa");
		d.findElementByClassName("decorativeSubmit").click();
		d.findElementByXPath("//div[@id='label']//following::a").click();
		d.findElementByXPath("//div[@id='left-content-column']//following::a").click();;
		d.findElementByXPath("//div[@id='left-content-column']//following::a[3]").click();
		
		WebElement table = d.findElementByClassName("x-panel-ml");
		List<WebElement> tableElements = table.findElements(By.tagName("td"));
		for(WebElement eachelement: tableElements) {
		System.out.println(eachelement.getText());
		}

	}

}
