package HomeWork;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TestCases {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver d=new ChromeDriver();
		d.manage().window().maximize();
		d.navigate().to("http://leaftaps.com/opentaps/control/main");
		d.findElementById("username").sendKeys("DemoSalesManager");
		d.findElementById("password").sendKeys("crmsfa");
		d.findElementByLinkText("Create Lead").click();
		d.findElementById("createLeadForm_companyName").sendKeys("ABC consultancies");
		d.findElementById("createLeadForm_firstName").sendKeys("pradeep");
		
		WebElement source = d.findElementById("createLeadForm_dataSourceId");
		Select sourceDroplist=new Select(source);
		sourceDroplist.selectByVisibleText("Employee");
		
		d.findElementById("createLeadForm_firstNameLocal").sendKeys("krishna");
		d.findElementById("createLeadForm_personalTitle").sendKeys("x---x");
		d.findElementById("createLeadForm_generalProfTitle").sendKeys("software engineer");
		
		d.findElementById("createLeadForm_annualRevenue").sendKeys("2 lacks");
		d.findElementByXPath("//input[@class='decorativeSubmit']").click();
		d.findElementByXPath("//div[@id='label']/a").click();
		d.findElementByLinkText("Leads").click();
		d.findElementByLinkText("Find Leads").click();
		d.findElementByLinkText("Phone").click();
		d.findElementByName("phoneCountryCode").clear();
		d.findElementByName("phoneCountryCode").sendKeys("191", Keys.TAB);
		d.findElementByName("phoneAreaCode").sendKeys("044 ",Keys.TAB);	
		d.findElementByName("phoneNumber").sendKeys("987654321",Keys.TAB);
		d.findElementByXPath("//button[@class='x-btn-text']").click();
		d.findElementByXPath("(//a[@class='linktext'])[4]").click();
		d.findElementByXPath("//a[text()='Delete']").click();
		

	}

}
