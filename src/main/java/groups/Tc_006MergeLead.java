package groups;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.internal.LocatingElementListHandler;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class Tc_006MergeLead extends ProjectMethods{

	@BeforeTest(groups= {"regression"})
	public void setData() {
		testCaseName="Tc_006MergeLead";
		testCaseDesc="";
		author="pradeep";
		category="regression";

	}
	@Test(groups= {"regression"},dataProvider="positive")

	public void mergeLead(String fromId,String toId ) {
		WebElement leads = locateElement("xpath", "//a[text()='Leads']");
		click(leads);
		WebElement mergeLeads = locateElement("xpath", "//a[text()='Merge Leads']");
		click(mergeLeads);
		WebElement fromButton = locateElement("xpath", "//td[@class='titleCell']//following::img");
		click(fromButton);

		switchToWindow(1);
		WebElement sendId = locateElement("xpath", "//input[@name='id']");
		type(sendId, fromId);
		WebElement findLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeads);
		WebElement firstId = locateElement("xpath", " //a[@class='linktext']");
		click(firstId);
		switchToWindow(0);
		WebElement toButton = locateElement("xpath","//td[@class='titleCell']//following::img[2]");
		click(toButton);
		switchToWindow(1);
		WebElement sendIdTo = locateElement("name", "id");
		type(sendIdTo, toId);
		WebElement findLeadsTo = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadsTo);
		WebElement firstIdTo = locateElement("xpath", "//a[@class='linktext']");
		click(firstIdTo);
		switchToWindow(0);
		WebElement merge = locateElement("xpath", "//a[text()='Merge']");
		click(merge);
		acceptAlert();
		WebElement findLeadsagain = locateElement("xpath","//a[text()='Find Leads']");
		click(findLeadsagain);



	}
	@DataProvider(name="positive")
	public Object[][] feedData() {
		Object [][]data=new Object[2][2];
		data[0][0]="103";
		data[0][1]="102";
		
		data[1][0]="103";
		data[1][1]="102";
		return data;
	}

}
