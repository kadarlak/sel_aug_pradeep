package groups;

import java.io.IOException;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import utils.ReadFromExcel;
import wdMethods.ProjectMethods;
import wdMethodsOld.SeMethods;

public class Tc002_CreateLead extends ProjectMethods{
	
	@BeforeTest(groups= {"smoke"})	
	public void setData() {
		testCaseName="Tc002_CreateLead";
		testCaseDesc="create a new lead";
		category="smoke";
		author="pradeep";
		excelFileName="CL.xlsx";
	}
	

	@Test(groups= {"smoke"}, dataProvider="fetchdata")
	public void createLead(String cName, String fName, String lName) throws InterruptedException {
		/*startApp("chrome", url);
		WebElement user = locateElement("id", "username");
		type(user, username);
		WebElement pass = locateElement("id","password");
		type(pass, password);
		WebElement login = locateElement("class","decorativeSubmit");
		
		click(login);
		
		WebElement crmLink = locateElement("linkText","CRM/SFA");
		click(crmLink);*/
		WebElement createLeadlink = locateElement("linkText","Create Lead");
		click(createLeadlink);
		WebElement cmpyName = locateElement("createLeadForm_companyName");
		type(cmpyName,cName);
		WebElement firstName = locateElement("createLeadForm_firstName");
		type(firstName,fName);
		WebElement lastname = locateElement("createLeadForm_lastName");
		type(lastname,lName);
		/*WebElement sourceDd = locateElement("createLeadForm_dataSourceId");
		selectDropDownUsingText(sourceDd, "Conference");
		WebElement firstNameLoc = locateElement("createLeadForm_firstNameLocal");
		type(firstNameLoc,"krishnaPradeep");
		WebElement salutation = locateElement("createLeadForm_personalTitle");
		type(salutation,"Dear");
		WebElement title = locateElement("createLeadForm_generalProfTitle");
		type(title,"softwareEngineer");
		WebElement annualInc = locateElement("name", "annualRevenue");
		type(annualInc,"3lcs");
		WebElement industry = locateElement("createLeadForm_industryEnumId");
		selectDropDownUsingText(industry, "Computer Software");
		WebElement ownership = locateElement("createLeadForm_ownershipEnumId");
		selectDropDownUsingText(ownership, "Partnership");
		WebElement sic = locateElement("createLeadForm_sicCode");
		type(sic,"521324");
		WebElement desc = locateElement("createLeadForm_description");
		type(desc,"employee details form");
		WebElement impnote = locateElement("createLeadForm_importantNote");
		type(impnote,"this field fills wth imp information");
		WebElement countryCode = locateElement("createLeadForm_primaryPhoneCountryCode");
		countryCode.clear();
		type(countryCode,"91");
		WebElement areaCodes = locateElement("createLeadForm_primaryPhoneAreaCode");
		type(areaCodes,"123");
		WebElement extn = locateElement("createLeadForm_primaryPhoneExtension");
		type(extn,"76547879");
		WebElement dept = locateElement("createLeadForm_departmentName");
		type(dept,"IT");
		WebElement currency = locateElement("createLeadForm_currencyUomId");
		selectDropDownUsingText(currency, "INR - Indian Rupee");
		WebElement noOfEmployees = locateElement("createLeadForm_numberEmployees");
		type(noOfEmployees,"55");
		WebElement tickerSymbol = locateElement("createLeadForm_tickerSymbol");
		type(tickerSymbol,"tng");
		WebElement askPerson = locateElement("createLeadForm_primaryPhoneAskForName");
		type(askPerson,"john cannedy");
		WebElement url1 = locateElement("createLeadForm_primaryWebUrl");
		type(url1,"www.hcl.com");
		WebElement toName = locateElement("createLeadForm_generalToName");
		type(toName,"krishna");
		WebElement addr1 = locateElement("createLeadForm_generalAddress1");
		type(addr1,"navalur");
		WebElement addr2 = locateElement("createLeadForm_generalAddress2");
		type(addr2,"OMR road-kanchipuram dist");
		WebElement city = locateElement("createLeadForm_generalCity");
		type(city,"chennai");
		WebElement country = locateElement("createLeadForm_generalCountryGeoId");
		selectDropDownUsingText(country,"India");
		WebElement stateProvince = locateElement("createLeadForm_generalStateProvinceGeoId");
		selectDropDownUsingText(stateProvince, "ANDHRA PRADESH");
		WebElement postalcode = locateElement("createLeadForm_generalPostalCode");
		type(postalcode,"603103");
		WebElement postalextn = locateElement("createLeadForm_generalPostalCodeExt");
		type(postalextn,"45");
		WebElement email = locateElement("createLeadForm_primaryEmail");
		type(email,"abc@gmail.com");*/
		WebElement createLead = locateElement("name", "submitButton");
		click(createLead);
		WebElement verifyFirstName = locateElement("viewLead_firstName_sp");
		WebDriverWait wait= new WebDriverWait(driver,10);
		
		
		verifyExactText(verifyFirstName, "Pradeep");
		
		
		

	}
	
	
	
	@DataProvider(name="negative")
	public void fetchData1() {
	}
	}


