package groups;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethodsOld.SeMethods;

public class Tc_005DuplicateLead extends ProjectMethods {
	
	@BeforeTest(groups= {"regression"})
	
	public void setData() {
		testCaseName="Tc_005DuplicateLead";
		testCaseDesc="duplicating the ID";
		author="pradeep";
		category="regression";
	}

	@Test(groups= {"regresion"},dataProvider="positive")
	public void editLead(String email, String name) throws InterruptedException {
		/*startApp("chrome", "http://leaftaps.com/opentaps/control/main");
		WebElement user = locateElement("id", "username");
		type(user, "DemoSalesManager");
		WebElement pass = locateElement("id","password");
		type(pass, "crmsfa");
		WebElement login = locateElement("class","decorativeSubmit");

		click(login);

		WebElement crmLink = locateElement("linkText","CRM/SFA");
		click(crmLink);*/
		WebElement leads = locateElement("xpath", "//a[text()='Leads']");
		click(leads);
		WebElement findLeads = locateElement("xpath", "//a[text()='Find Leads']");
		click(findLeads);
		WebElement emailbutton = locateElement("xpath", "//span[text()='Email']");
		click(emailbutton);
		WebElement email1 = locateElement("name", "emailAddress");
		type(email1,email);
		WebElement findLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadsButton);
		Thread.sleep(5000);
		WebElement firstRecordId = locateElement("xpath", "(//a[@class='linktext'])[4]");
		WebElement nameOfResultngId = locateElement("xpath", "(//a[@class='linktext'])[6]");
		getText(nameOfResultngId);
		click(firstRecordId);
		WebElement duplicateId = locateElement("xpath", "//a[text()='Duplicate Lead']");
		click(duplicateId);
		//WebElement title = locateElement("xpath", "//title[text()='Duplicate Lead | opentaps CRM']");
		verifyTitle("Duplicate Lead | opentaps CRM");
		WebElement createLead = locateElement("xpath", "//input[@name='submitButton']");
		click(createLead);
		WebElement duplicateRecordName = locateElement("viewLead_firstName_sp");
		verifyExactText(duplicateRecordName, name);
		
		// yet to create test case for Confirm the duplicated lead name is same as captured name
		closeBrowser();
	}
	
	@DataProvider(name="positive")
	public Object[][] feedData() {
		Object[][] data=new Object[2][2];
		
		data[0][0]="abc@gmail.com";
		data[0][1]="pradeep";
		
		data[0][0]="abc@gmail.com";
		data[0][1]="Gayatri";
		return data;
	}
}
