package groups;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethodsOld.SeMethods;

public class Tc_004DeleteLead extends ProjectMethods {
	
	@BeforeTest
	
	public void setData() {
		testCaseName="Tc_004DeleteLead";
		testCaseDesc="deleting the lead";
		author="pradeep";
		category="smoke";
	}

	@Test(dataProvider="positive")
	public void deleteLead(String phNo) throws InterruptedException {
		/*startApp("chrome", "http://leaftaps.com/opentaps/control/main");
		WebElement user = locateElement("id", "username");
		type(user, "DemoSalesManager");
		WebElement pass = locateElement("id","password");
		type(pass, "crmsfa");
		WebElement login = locateElement("class","decorativeSubmit");

		click(login);

		WebElement crmLink = locateElement("linktext","CRM/SFA");
		click(crmLink);*/
		WebElement leads = locateElement("xpath", "//a[text()='Leads']");
		click(leads);
		WebElement findLeads = locateElement("xpath", "//a[text()='Find Leads']");
		click(findLeads);
		WebElement phone = locateElement("xpath", "//span[text()='Phone']");
		click(phone);
		WebElement number = locateElement("name", "phoneNumber");
		type(number,phNo);
		WebElement findLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadsButton);
		Thread.sleep(5000);
		//WebElement frst = locateElement("xpath", "(//a[@class='linktext'])[4]");
		WebElement firstRecordId = locateElement("xpath", "(//a[@class='linktext'])[4]");
		String text = firstRecordId.getText();
		click(firstRecordId);
		WebElement delete = locateElement("xpath", "//a[text()='Delete']");
		click(delete);
		
		Thread.sleep(3000);
		WebElement clickFindLeads = locateElement("xpath", "//a[text()='Find Leads']");
		click(clickFindLeads);
		WebElement enterLeadId = locateElement("xpath","//input[@name='id']");
		type(enterLeadId, text);
		WebElement clickFindButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(clickFindButton);
		WebElement errormsg = locateElement("xpath", "//div[text()='No records to display']");
		verifyExactText(errormsg, "No records to display");
		
		closeBrowser();

	}
	
	@DataProvider(name="positive")
	public Object[][]feedData() {
		Object[][]data=new Object[2][3];
		data[0][0]="76547879";
		data[0][1]="765";
		return data;
	}
}
