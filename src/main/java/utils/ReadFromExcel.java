package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadFromExcel {

	public static Object[][] readFromExcel(String excelFileName) throws IOException {
		
		//create an object for xsssfworkbook sheet and specify path
		XSSFWorkbook workbook=new XSSFWorkbook("./data/"+excelFileName+".xlsx");
		
		//get sheet based on index(always best practice) though we can do on name based
		XSSFSheet sheetAt = workbook.getSheetAt(0);
		
		//XSSFSheet sheet = workbook.getSheet("sheet1");
		
		//get the number of last row--to know how many rows 
		int lastRowNum = sheetAt.getLastRowNum();
		System.out.println(lastRowNum);
		
		//know the last cell value to know until what value we need to iterate
		int lastCellNum = sheetAt.getRow(0).getLastCellNum();
		System.out.println(lastCellNum);
		
		Object[][]data=new Object[lastRowNum][lastCellNum];
		
		//first for loop is to iterate until the last row number and get each row also "i" starts from 1 as we dont need header
		for(int i=1;i<=lastRowNum;i++) {
			XSSFRow row = sheetAt.getRow(i);
			
			//the below for loop is to iterate until the last workbook
			for(int j=0;j<lastCellNum;j++) {
				XSSFCell cell = row.getCell(j);
				
				data[i-1][j] = cell.getStringCellValue();
				System.out.println(data[i-1][j] +"\n");
			}
		}
		return data;
		
		

	}

}
