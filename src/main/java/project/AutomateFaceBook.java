package project;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethodsOld.SeMethods;

public class AutomateFaceBook extends SeMethods{

	@Test
	public void faceBook() throws InterruptedException {
		startApp("chrome", "https://www.facebook.com/");
		WebElement username = locateElement("email");
		username.clear();
		type(username, "kadarla.pradeep4@gmail.com");
		WebElement password = locateElement("pass");
		type(password, "pradeep5");
		WebElement login = locateElement("xpath", " //label[@class='uiButton uiButtonConfirm']/input");
		click(login);
		Thread.sleep(5000);
		WebElement search = locateElement("xpath", "//input[@name='q']");
		type(search, "TestLeaf");
		WebElement suggestions = locateElement("xpath", "//div[text()='testleaf']");
		verifyDisplayed(suggestions);
		WebElement searchbuton = locateElement("xpath", "//i[@class='_585_']");
		click(searchbuton);
		WebElement likebutton = locateElement("xpath", "//button[@class='_42ft _4jy0 PageLikeButton _4jy3 _517h _51sy']");
		verifySelected(likebutton);
		String text = likebutton.getText();
		if(text.equalsIgnoreCase("Like")) {
			click(likebutton);
			WebElement tLlink = locateElement("xpath", "//a[@class='_2yez']/div");
			click(tLlink);
		}else {
			System.out.println("page was already liked");
			WebElement tLlink = locateElement("xpath", "//a[@class='_2yez']/div");
			click(tLlink);
		}
	WebElement title = locateElement("xpath", "//title[text()='TestLeaf � Facebook Search']");
	String webTitle = title.getText();
	
	verifyPartialText(title, "TestLeaf");
	System.out.println("the title of the webpage"+webTitle+" contains TestLeaf");
		WebElement likes = locateElement("xpath", "( //div[@class='_4bl9'])[3]/div");
		String text2 = likes.getText().replaceAll("\\D", "");
		int noOfLikes = Integer.parseInt(text2);
		System.out.println(noOfLikes);
		closeBrowser();

	}

}
