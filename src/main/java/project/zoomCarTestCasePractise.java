package project;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethodsOld.SeMethods;

public class zoomCarTestCasePractise extends SeMethods {

	@Test

	public void zoomCar() {
		startApp("chrome", "https://www.zoomcar.com/chennai");
		WebElement journey = locateElement("xpath","//a[@title='Start your wonderful journey']");
		click(journey);
		WebElement pickPoint = locateElement("xpath", "//div[contains(text(),'Thuraipakkam')]");
		click(pickPoint);
		WebElement next = locateElement("xpath","//button[text()='Next']");
		click(next);
		WebElement date = locateElement("xpath", "//div[text()='Mon']");
		click(date);
		WebElement next2 = locateElement("xpath", "//button[@class='proceed']");
		click(next2);
		WebElement done = locateElement("xpath", "//button[text()='Done']");
		click(done);
		List<WebElement> carResults = locateElements("xpath","//div[@class='car-listing']"); 
		System.out.println(carResults.size());
		List<WebElement> price = locateElements("xpath", "//div[@class='price']");
		List<Integer> priceList=new ArrayList<Integer>();
		for(WebElement eachPrice: price) {
			String text = eachPrice.getText().replaceAll("\\D", "");
			int priceNumber = Integer.parseInt(text);
			priceList.add(priceNumber);
		}
		Integer max = Collections.max(priceList);
		System.out.println(max);
		
		WebElement brandName = locateElement("xpath", "//*[contains(text(),'"+max+"')]/preceding::h3");
		System.out.println(brandName.getText());
		WebElement bookNow = locateElement("xpath", "//*[contains(text(),'"+max+"')]/following::button");
		click(bookNow);
		closeBrowser();
		
		
		
	}

}
