package week1.day1;

import java.util.Scanner;

public class SumOfOddNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("enter the value of n1");
		Scanner s=new Scanner(System.in);
		int n1=s.nextInt();
		int n2=s.nextInt();
		int sum=0;
		
		for(int i=n1;i<=n2;i++) {
			if(i%2==1) {
				sum=sum+i;
			}
		}
		System.out.println("Sum of odd numbers is: "+sum);

	}

}
