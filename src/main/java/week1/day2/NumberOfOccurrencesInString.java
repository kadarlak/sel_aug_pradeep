package week1.day2;

import java.util.Scanner;

public class NumberOfOccurrencesInString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int count=0;
		
		//String s=new String();
		Scanner s1=new Scanner(System.in);
		String input= s1.nextLine();
		char [] s2=input.toCharArray();
		
		for(char eachchar:s2) {
			if(eachchar=='a'||eachchar=='A') {
				count++;
			}
		}
		System.out.println(count);

	}

}
