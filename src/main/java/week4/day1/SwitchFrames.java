package week4.day1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SwitchFrames {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver d=new ChromeDriver();
		d.navigate().to("https://jqueryui.com/selectable/");
		WebDriver frame = d.switchTo().frame("//iframe[@class='demo-frame']");
		d.findElementByXPath("//li[text()='Item 4']").click();
		
		

	}

}
