package week4.day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlertAndFrames {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver d=new ChromeDriver();
		d.navigate().to("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		WebDriver frame = d.switchTo().frame("iframeResult");
		d.findElementByXPath("//button[text()='Try it']").click();
		Alert alert = d.switchTo().alert();
		System.out.println(alert.getText());
		alert.sendKeys("pradeep");
		alert.accept();
		String text = d.findElementById("demo").getText();
		System.out.println(text);
		d.close();
		
		
		

	}

}
