package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindows {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver d=new ChromeDriver();
		
		d.manage().window().maximize();
		d.navigate().to("https://www.irctc.co.in/nget/train-search");
		System.out.println(d.getTitle());
		
		d.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		d.findElementByLinkText("Contact Us").click();
		Set<String> windowHandles = d.getWindowHandles();
		System.out.println(windowHandles);
		List<String> wh=new ArrayList<String>(windowHandles);
		d.switchTo().window(wh.get(1));
		System.out.println(d.getTitle());
		System.out.println(d.getCurrentUrl());
		
		File screenshotAs = d.getScreenshotAs(OutputType.FILE);
		File desc=new File("./snaps/img.png");
		FileUtils.copyFile(screenshotAs, desc);
		d.switchTo().window(wh.get(0)).close();
		
		
		

	}

}
