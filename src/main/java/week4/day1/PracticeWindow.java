package week4.day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class PracticeWindow {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver d =new ChromeDriver();
		d.navigate().to("https://www.flipkart.com/");
		d.getKeyboard().sendKeys(Keys.ESCAPE);
		WebElement wl = d.findElementByXPath("//span[text()='Electronics']");
		Thread.sleep(5000);
		WebElement descElement = d.findElementByLinkText("Apple");
		
		Actions builder=new Actions(d);
		builder.moveToElement(wl).click(descElement).click().perform();
	}

}
