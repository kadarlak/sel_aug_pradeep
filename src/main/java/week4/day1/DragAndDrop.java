package week4.day1;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DragAndDrop {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver d=new ChromeDriver();
		
		d.manage().window().maximize();
		d.navigate().to("https://jqueryui.com/draggable/");
	//	WebElement xpath = d.findElementByXPath("//iframe[@class='demo-frame']");
		d.switchTo().frame(0);
		WebElement drag = d.findElementById("draggable");
		Point location = drag.getLocation();
		System.out.println(location);
		
		Actions builder=new Actions(d);
		
		
		builder.dragAndDropBy(drag,25,55).perform();
		

	}

}
