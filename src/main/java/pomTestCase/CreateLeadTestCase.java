package pomTestCase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class CreateLeadTestCase extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		testCaseName="CreateLeadTestCase";
		testCaseDesc="login and logout";
		author="pradeep";
		category="smoke";
		excelFileName="LoginLogout";
	}
	
	@Test(dataProvider="fetchdata")
	public void createLead(String uName, String pwd, String verifyLoggedInName, String companyName, String firstName, String lastName) {
		new LoginPage().typeUserName(uName).typePassword(pwd).clickLogin().clickLink().clickLink().createLead().typeComapnyName(companyName).typeFirstName(firstName).typeLastName(lastName).clickSubmit();
		
		
	}
	
	

}
