package pomTestCase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class LoginTestCase extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName="LoginTestCase";
		testCaseDesc="login and logout";
		author="pradeep";
		category="smoke";
		excelFileName="LoginLogout";
	}
	@Test(dataProvider="fetchdata")
	public void loginLogout(String userName, String password, String verifyLoggedInName) {
		LoginPage lp=new LoginPage();
		lp.typeUserName(userName).typePassword(password).clickLogin().verifyUserName(verifyLoggedInName);
		
		
	}

}
