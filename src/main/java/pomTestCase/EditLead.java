package pomTestCase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class EditLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName="EditLead";
		testCaseDesc="this testcase will edit the lead record";
		author="pradeep";
		category="smoke";
		excelFileName="EL";
	}

	@Test(dataProvider="fetchdata")
	public void editLead(String uName, String pwd, String username, String newCompanyName,String verifynewCompanyName) throws InterruptedException {
		new LoginPage().typeUserName(uName).typePassword(pwd).clickLogin().clickLink().clickLink().clickFindLead().typeLeadName(username)
		.findLeadsButton().clickFisrtRecord().clickEditLead().editCompanyName(newCompanyName).clickUpdate().verifyNewCname(verifynewCompanyName);
	}

}
