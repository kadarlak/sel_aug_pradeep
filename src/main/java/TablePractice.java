import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.activation.FileTypeMap;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TablePractice {

	public static void main(String[] args) throws WebDriverException, IOException {
		
		 List<String> l=new ArrayList<String>();
		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.navigate().to("http://leafground.com/pages/table.html");
		List<WebElement> allElements = driver.findElementsByCssSelector("*");
		for(WebElement eachElement: allElements) {
			String text = eachElement.getTagName();
			String value=eachElement.getText();
			// System.out.println(text);
			 l.add(text);
			 if(text.contains("table")) {
				 System.out.println(value);
			 }
		}
		List<WebElement> checkboxes = driver.findElementsByName("vital");
		checkboxes.get(checkboxes.size()-2).click();
		for(WebElement items: checkboxes) {
			String text = items.getText();
			l.add(text);
			int size = l.size();
			System.out.println(size);
			
		}
	WebElement tableelements = driver.findElementByTagName("table");
	List<WebElement> rows = tableelements.findElements(By.tagName("tr"));
	List<WebElement> columns = tableelements.findElements(By.tagName("td"));
	for(WebElement eachrow:rows) {
		//eachrow.findElement(By.tagName("td"));
		String text = eachrow.getText();
		System.out.println(text);
		
		
	}
	for(WebElement eachColumns:columns) {
		//eachColumns.findElement(By.tagName("td"));
		String text = eachColumns.getText();
		System.out.println(text);
		FileUtils.copyFile(driver.getScreenshotAs(OutputType.FILE), new File("./snaps/picture.png"));
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(eachColumns));
		
		
		

	}
	}
}


