package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnHtmlReports {

	public static void main(String[] args) throws IOException {
		//to create html report
		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/result.html");
		//to save the history of the previous executions and preserve the data, if we make it false, it wll not preserve the previous o/p
		html.setAppendExisting(true);
		//to convert that test report in editable format
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);
		
		//testcase level
		ExtentTest test=extent.createTest("TC001_LoginAndLogOut", "logon to create lead and logout");
		test.assignCategory("smoke");
		test.assignAuthor("pradeep");
		
		//test step level, this will give status in words and add the screenshot underneath the text
		test.pass("browser has been launched succesfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.pass("username demosalesmanager enetered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
		test.pass("password crmsfa enetered successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img3.png").build());
		test.pass("succesfully clicked on login button", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img4.png").build());
		
		//w/o below line this code won't run this will flush the memory and execute the extent methods
		extent.flush();
		
	

	}

}
