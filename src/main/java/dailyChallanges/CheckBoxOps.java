package dailyChallanges;

import org.openqa.selenium.chrome.ChromeDriver;

public class CheckBoxOps {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver cd=new ChromeDriver();
		cd.manage().window().maximize();
		cd.get("http://testleaf.herokuapp.com/pages/checkbox.html");
		boolean status = cd.findElementByXPath("//label[contains(text(),'Confirm Selenium is checked')]//following::input").isSelected();
		System.out.println(status);
		cd.findElementByXPath("//label[contains(text(),'Confirm Selenium is checked')]//following::input").click();

	}

}
