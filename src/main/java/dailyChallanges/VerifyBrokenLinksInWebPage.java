package dailyChallanges;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class VerifyBrokenLinksInWebPage {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
	System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://www.google.co.in/");
		List<WebElement> links = driver.findElements(By.tagName("a"));
		for(WebElement allLinks:links) {
			String url = allLinks.getAttribute("href");
			URL url1=new URL(url);
			HttpURLConnection httpconnection= (HttpURLConnection)url1.openConnection();
			httpconnection.setConnectTimeout(3000);
			httpconnection.connect();
			if(httpconnection.getResponseCode()==200) {
				System.out.println(url+"-"+httpconnection.getResponseMessage());
			}
			if(httpconnection.getResponseCode()==201) {
				System.out.println(url+"-"+httpconnection.getResponseMessage()+"--"+httpconnection.HTTP_CREATED);
			}
			if(httpconnection.getResponseCode()==202) {
				System.out.println(url+"-"+httpconnection.getResponseMessage()+"--"+httpconnection.HTTP_ACCEPTED);
			}
			if(httpconnection.getResponseCode()==400) {
				System.out.println(url+"-"+httpconnection.getResponseMessage()+"--"+httpconnection.HTTP_BAD_REQUEST);
			}
			if(httpconnection.getResponseCode()==401) {
				System.out.println(url+"-"+httpconnection.getResponseMessage()+"--"+httpconnection.HTTP_UNAUTHORIZED);
			}
			if(httpconnection.getResponseCode()==402) {
				System.out.println(url+"-"+httpconnection.getResponseMessage()+"--"+httpconnection.HTTP_PAYMENT_REQUIRED);
			}
			if(httpconnection.getResponseCode()==403) {
				System.out.println(url+"-"+httpconnection.getResponseMessage()+"--"+httpconnection.HTTP_FORBIDDEN);
			}
			if(httpconnection.getResponseCode()==404) {
				System.out.println(url+"-"+httpconnection.getResponseMessage()+"--"+httpconnection.HTTP_NOT_FOUND);
			}
		}

	}

}
