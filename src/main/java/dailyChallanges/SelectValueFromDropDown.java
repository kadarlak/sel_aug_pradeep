package dailyChallanges;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelectValueFromDropDown {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver cd=new ChromeDriver();
		cd.manage().window().maximize();
		cd.get("http://leafground.com/pages/Dropdown.html");
		WebElement drop = cd.findElementById("dropdown1");
		Select s=new Select(drop);
		s.selectByValue("1");
		//s.findElement(By.xpath("//option[contains(text(),'UFT/QTP')]")).;

	}

}
