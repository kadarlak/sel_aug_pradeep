package dailyChallanges;

public class MultiplesOf3And5 {

	public static void main(String[] args) {
		
		int sum=0;
		for(int i=0;i<100;i++ ) {
			if(i%3==0||i%5==0) {
				sum=sum+i;
				//System.out.println(i);
			}else {
				continue;
			}
		}
		System.out.println("Sum of the numbers divisible by 3 and 5 are: "+sum);

	}

}
