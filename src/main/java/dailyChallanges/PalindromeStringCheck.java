package dailyChallanges;

import java.util.Scanner;

public class PalindromeStringCheck {

	public static void main(String[] args) {
		System.out.println("enter a string input:");
		Scanner s=new Scanner(System.in);
		String input = s.nextLine();
		String rev="";
		
		for(int i=input.length()-1;i>=0;i--) {
			rev=rev+input.charAt(i);
		}
		System.out.println(rev);
		
		if(rev.equalsIgnoreCase(input)) {
			System.out.println("given string is a palindrome");
			
		}
		else
			System.out.println("not a palindrome string");

	}

}
