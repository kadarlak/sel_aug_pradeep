package dailyChallanges;

import java.util.Scanner;

public class SwappingNumbersWithoutThirdVar {

	public static void main(String[] args) {
		System.out.println("enter two numbers");
		Scanner s=new Scanner(System.in);
		int a = s.nextInt();
		int b = s.nextInt();
		System.out.println("before swap"+ a+" "+b);
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("After swap"+ a+" "+b);
	}

}
