package dailyChallanges;

import java.util.Scanner;

public class SumOfNumbersInArray {

	public static void main(String[] args) {
		System.out.println("enter the size of an array");
		Scanner s=new Scanner(System.in);
		int n = s.nextInt();
		int sum=0;
		int [] arr=new int[n];
		System.out.println("enter elements of an array");
		for(int i=0;i<n;i++) {
			arr[i]=s.nextInt();
		}
		
		for(int elements: arr) {
			
			sum+=elements;
		}
		System.out.println("sum of elements of an array are "+sum);

	}

}
