package dailyChallanges;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class NumberOfOptionsInDropDown {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver d= new ChromeDriver();
		d.manage().window().maximize();
		d.get("http://leafground.com/pages/Dropdown.html");
		WebElement dropdown = d.findElement(By.id("dropdown1"));
		Select droplist=new Select(dropdown);
		List<WebElement> options = droplist.getOptions();
		int size = options.size();
		System.out.println(size);
		for(WebElement total: options) {
			System.out.println(total.getText());
		}
		

	}

}
