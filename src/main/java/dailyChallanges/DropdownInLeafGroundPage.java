package dailyChallanges;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropdownInLeafGroundPage {

	public static void main(String[] args) {
		
		ArrayList<String>l=new ArrayList<String>();
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver d=new ChromeDriver();
		d.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		d.get("http://leafground.com/pages/Dropdown.html");
		d.manage().window().maximize();
		WebElement dropdown = d.findElementById("dropdown3");
		Select course=new Select(dropdown);
		
		List<WebElement> courseList = course.getOptions();
		int size = courseList.size();
		courseList.get(size-1).click();
		for(WebElement e:courseList) {
			System.out.println(e.getText());
		
		
		}
		d.close();
		
	

	}

}
