package dailyChallanges;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class CheckboxVerification {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver d=new ChromeDriver();
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		d.get("http://leafground.com/pages/checkbox.html");
		boolean b = d.findElementByXPath("//div[@class='example']/following::input").isSelected();
		System.out.println(b);
		System.out.println(d.findElementByXPath("//div[@id='contentblock']//following::input").isSelected());
		
		d.close();

	}

}
