package dailyChallanges;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class FindCounryIrctc {

	public static void main(String[] args) {
		ArrayList<String> l=new ArrayList<String>();
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver d= new ChromeDriver();
		d.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		WebElement country = d.findElementById("userRegistrationForm:countries");
		Select countryDropDown=new Select(country);
		
		List<WebElement> countrylist = countryDropDown.getOptions();
		
		for(WebElement list: countrylist) {
			String text = list.getText();
			
			if(text.startsWith("E")) {
				l.add(text);
			}
			
		}
		String cname = l.get(1);
		countryDropDown.selectByVisibleText(cname);
		

	}

}
