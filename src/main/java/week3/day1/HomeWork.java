package week3.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomeWork {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		
		ChromeDriver d=new ChromeDriver();
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		d.manage().window().maximize();
		d.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		d.findElementById("userRegistrationForm:userName").sendKeys("pradeep");
		d.findElementById("userRegistrationForm:password").sendKeys("abc");
		d.findElementById("userRegistrationForm:confpasword").sendKeys("abc");
		WebElement e=d.findElementById("userRegistrationForm:securityQ");
		
		Select securityDropdown=new Select(e);
		securityDropdown.selectByValue("0");
		d.findElementById("userRegistrationForm:securityAnswer").sendKeys("dog");
		WebElement languageElement = d.findElementById("userRegistrationForm:prelan");
		Select langDropdown=new Select(languageElement);
		langDropdown.selectByValue("en");
		d.findElementById("userRegistrationForm:firstName").sendKeys("abc");
		d.findElementById("userRegistrationForm:middleName").sendKeys("def");
		d.findElementById("userRegistrationForm:lastName").sendKeys("ghi");
		d.findElementById("userRegistrationForm:gender:0").click();
		d.findElementById("userRegistrationForm:maritalStatus:1").click();
		WebElement date = d.findElementById("userRegistrationForm:dobDay");
		Select dateInDob=new Select(date);
		dateInDob.selectByValue("01");
		WebElement monthInDob = d.findElementById("userRegistrationForm:dobMonth");
		Select month=new Select(monthInDob);
		month.selectByValue("03");
		WebElement yearInDob = d.findElementById("userRegistrationForm:dateOfBirth");
		Select year=new Select(yearInDob);
		year.selectByValue("1996");
		
		WebElement occupationElement = d.findElementById("userRegistrationForm:occupation");
		Select occupDropdown=new Select(occupationElement);
		occupDropdown.selectByVisibleText("Government");
		
		d.findElementById("userRegistrationForm:uidno").sendKeys("45673234577");
		d.findElementById("userRegistrationForm:idno").sendKeys("dshp65868");
		
		WebElement countryelement = d.findElementById("userRegistrationForm:countries");
		Select countryDropDown=new Select(countryelement);
		countryDropDown.selectByVisibleText("India");
		
		d.findElementById("userRegistrationForm:email").sendKeys("abc@gmail.com");
		d.findElementById("userRegistrationForm:mobile").sendKeys("876545788");
		
		WebElement nationalityElement = d.findElementById("userRegistrationForm:nationalityId");
		Select nationalityDropdown=new Select(nationalityElement);
		nationalityDropdown.selectByValue("94");
		
		d.findElementById("userRegistrationForm:address").sendKeys("olympia");
		d.findElementById("userRegistrationForm:street").sendKeys("navalur");
		d.findElementById("userRegistrationForm:area").sendKeys("kanchipuram");
		d.findElementById("userRegistrationForm:pincode").sendKeys("603103", Keys.TAB);
		
		Thread.sleep(5000);
		WebElement cityElement = d.findElementByXPath("//select[@name='userRegistrationForm:cityName']");
		Select CityDropDown=new Select(cityElement);
		CityDropDown.selectByValue("Kanchipuram");
		Thread.sleep(5000);
		
		WebElement postElement = d.findElementById("userRegistrationForm:postofficeName");
		Select postDropDown=new Select(postElement);
		postDropDown.selectByValue("Kelambakkam S.O");
		
		d.findElementById("userRegistrationForm:landline").sendKeys("10171551");
		//WebDriverWait wait=new WebDriverWait(d,10);
		//wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='userRegistrationForm:resAndOff'])[2]")));
		d.findElementById("userRegistrationForm:resAndOff:1").click();
	//	Thread.sleep(2000);
		d.findElementByXPath("(//input[@name='userRegistrationForm:resAndOff'])[2]");
		d.findElementById("userRegistrationForm:addresso").sendKeys("amber-15c1");
		d.findElementById("userRegistrationForm:streeto").sendKeys("omr road");
		d.findElementByName("userRegistrationForm:areao").sendKeys("near AGS");
		WebElement countryInOffice = d.findElementById("userRegistrationForm:countrieso");
		Select countryO=new Select(countryInOffice);
		countryO.selectByValue("94");
		d.findElementById("userRegistrationForm:pincodeo").sendKeys("603103",Keys.TAB);
		Thread.sleep(5000);
		WebElement cityElementO = d.findElementById("userRegistrationForm:cityNameo");
		Select CityDropDownO=new Select(cityElementO);
		CityDropDownO.selectByVisibleText("Kanchipuram");
		Thread.sleep(5000);
		
		WebElement postOfficeElement = d.findElementById("userRegistrationForm:postofficeNameo");
		Select postOfficeDropdown=new Select(postOfficeElement);
		postOfficeDropdown.selectByVisibleText("Navalur B.O");
		d.findElementById("userRegistrationForm:landlineo").sendKeys("9997066465");
		
				
				
		
		
		

	}

	

}
