package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.internal.runners.model.EachTestNotifier;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		
		ChromeDriver driver=new ChromeDriver();	
		
		try {
			
			driver.manage().window().maximize();
			
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.get("http://leaftaps.com/opentaps/");
			
			try {
				driver.findElementById("username").sendKeys("DemoSalesManager");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			
			driver.findElementById("password").sendKeys("crmsfa");
			driver.findElementByClassName("decorativeSubmit").click();
			driver.findElementByLinkText("CRM/SFA").click();
			driver.findElementByLinkText("Create Lead").click();
			driver.findElementById("createLeadForm_companyName").sendKeys("xxx");
			driver.findElementById("createLeadForm_firstName").sendKeys("federer");
			driver.findElementById("createLeadForm_lastName").sendKeys("roger");

			WebElement element = driver.findElementById("createLeadForm_dataSourceId");
			Select dropdown=new Select(element);
			dropdown.selectByVisibleText("Direct Mail");
			
			WebElement marketElement=driver.findElementById("createLeadForm_marketingCampaignId");
			  Select marketDropDown=new Select(marketElement);
			  List<WebElement> options = marketDropDown.getOptions();
			  for(WebElement eachoption: options) {
				  System.out.println(eachoption.getText());
			  }
			  marketDropDown.selectByIndex((options.size()-2));
			//driver.findElementByClassName("smallSubmit").click();
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("excepton has been caught");
		//	throw new RuntimeException();
		}
		
		finally {
			driver.close();
		}
		boolean a=true, b=false;
		
		if(a&&!b)
		{
			
		}
	}
}
		
