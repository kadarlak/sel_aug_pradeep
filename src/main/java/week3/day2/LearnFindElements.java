package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFindElements {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		
		ChromeDriver d=new ChromeDriver();
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		d.manage().window().maximize();
		d.get("http://leafground.com/pages/table.html");
		d.navigate().to("");
		List<WebElement> checkboxes = d.findElementsByXPath("//input[@type='checkbox']");
		int size = checkboxes.size();
		System.out.println(size);
	
		checkboxes.get(size-1).click();
		
		WebElement table = d.findElementByXPath("//section[@class='innerblock']//table");
		List<WebElement> findElement = table.findElements(By.tagName("tr"));
		System.out.println(findElement.size());
		for(WebElement eachElement:findElement) {
			String text = eachElement.getText();
			System.out.println(text);
			if(text.contains("80%")) {
				table.findElement(By.xpath("//font[contains(text(),'80%')]/following::input")).click();
				d.close();
			}
		}
			
		
		/*WebElement webElement = findElement.get(1);
		List<WebElement> tabledata = webElement.findElements(By.tagName("td"));
		int size2 = tabledata.size();
		System.out.println(size2);
		System.out.println(tabledata.get(1).getText());*/
		
		
		
		

	}

}
