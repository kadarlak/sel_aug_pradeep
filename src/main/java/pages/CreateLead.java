package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {
	
	public CreateLead() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.ID,using="createLeadForm_companyName")
	WebElement cName;
	public CreateLead typeComapnyName(String companyName) {
		type(cName, companyName);
		return this;
		
	}

	@FindBy(how=How.ID,using="createLeadForm_firstName")
	WebElement fName;
	public CreateLead typeFirstName(String firstName) {
		type(fName, firstName);
		return this;
		
	}
	@FindBy(how=How.ID,using="createLeadForm_lastName")
	WebElement lName;
	public CreateLead typeLastName(String lastName ) {
		type(lName, lastName);
		return this;
		
	}
	@FindBy(how=How.CLASS_NAME, using="smallSubmit")
	WebElement sumbit;
	public ViewLead clickSubmit() {
		click(sumbit);
		return new ViewLead();
	}
}
