package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeads extends ProjectMethods{
	
	public FindLeads() {
		
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="(//input[@name='firstName'])[3]" )
	WebElement lead;
	public FindLeads typeLeadName(String leadName) {
		type(lead, leadName);
		return this;
	}
	
	@FindBy(how=How.XPATH, using="//span[text()='Phone']")
	WebElement phoneclick;
	public FindLeads clickPhone() {
		click(phoneclick);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//input[@name='phoneNumber']")
	WebElement phoneNumber;
	public FindLeads typePhoneNumber(String number) {
		type(phoneNumber, number);
		return this;
		
	}
	
	@FindBy(how=How.XPATH,using="(//a[@class='linktext'])[4]")
	WebElement firstPhone;
	public ViewLead clickFirstPhoneRecord() {
		click(firstPhone);
		return new ViewLead();
	}
	
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']")
	WebElement findButtton;
	public FindLeads findLeadsButton() throws InterruptedException {
		click(findButtton);
		Thread.sleep(3000);
		return this;
		
	}
	
	@FindBy(how=How.XPATH, using="(//a[@class='linktext'])[4]")
	WebElement firstId;
	public ViewLead clickFisrtRecord() {
		click(firstId);
		
		return new ViewLead();
	}

}
