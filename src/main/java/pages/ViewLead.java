package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLead extends ProjectMethods{
	
	public ViewLead() {
		PageFactory.initElements(driver, this);
	}
	
	
	public ViewLead verifyTitle() {
		verifyTitle();
		return this;
		
	}
	
	@FindBy(how=How.XPATH,using="//a[text()='Edit']")
	WebElement edit;
	public OpenTapsCRMeditLead clickEditLead() {
		click(edit);
		return new OpenTapsCRMeditLead();
	}
	
	@FindBy(how=How.ID,using="viewLead_companyName_sp")
	WebElement newCname;
	public void verifyNewCname(String newCompanyName) {
		verifyExactText(newCname, newCompanyName);
	}

}
