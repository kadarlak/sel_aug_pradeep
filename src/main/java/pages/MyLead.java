package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLead extends ProjectMethods {


	public MyLead() {
		PageFactory.initElements(driver, this);
	}


	@FindBy(how =How.XPATH, using="//a[text()='Create Lead']")
	WebElement createLink;

	public CreateLead createLead() {

		click(createLink);
		return new  CreateLead();

	}
	@FindBy(how=How.XPATH, using="//a[text()='Find Leads']")
	WebElement findleads;
	public FindLeads clickFindLead() {
		click(findleads);
			return new FindLeads();
	}


}
