package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;
import week3.day1.Login;

public class LoginPage extends ProjectMethods{
	
	

	public LoginPage() {
		
		PageFactory.initElements(driver, this);
		//PageFactory.initElements(driver, this);

	}
	
	@FindBy(id="username")
	WebElement eleUserName;
	public LoginPage typeUserName(String uName) {
		
	//	WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, uName);
		return this;
		
	}
	
	@FindBy(id="password")
	WebElement elePwd;
	public LoginPage typePassword(String pwd) {
		//WebElement elePwd = locateElement("id", "password");
		type(elePwd, pwd);
		return this;
	}
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit")
	WebElement eleLogin;
	public HomePage clickLogin() {
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		return new HomePage();
		
	}
	
	

}
