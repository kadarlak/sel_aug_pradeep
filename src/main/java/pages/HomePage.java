package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods {
	
	public HomePage() {
		
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how =How.XPATH, using="//*[text()[contains(.,'Demo Sales Manager')]]")
	WebElement eleVerifyName;
	public HomePage verifyUserName(String verifyLoggedInName) {
		verifyExactText(eleVerifyName,verifyLoggedInName);
		
		return this;
		
	}
	
	@FindBy(how=How.XPATH, using="//div[@id='label']/a")
	WebElement eleClick;
	public MyHome clickLink() {
		click(eleClick);
		return new MyHome();
	}
	
	

}
