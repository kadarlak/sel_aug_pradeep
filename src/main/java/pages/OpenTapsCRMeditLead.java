package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class OpenTapsCRMeditLead extends ProjectMethods{
	
	public OpenTapsCRMeditLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID ,using="updateLeadForm_companyName")
	WebElement editCname;
	public OpenTapsCRMeditLead editCompanyName(String newCname) {
		type(editCname, newCname);
		return this;
	}
	
	@FindBy(how=How.NAME,using="submitButton")
	WebElement update;
	public ViewLead clickUpdate() {
		click(update);
		return new ViewLead();
		
	}

}
