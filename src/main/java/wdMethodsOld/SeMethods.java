package wdMethodsOld;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.http.impl.client.NoopUserTokenHandler;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.annotations.Until;

import week4.day1.DragAndDrop;

public class SeMethods implements WdMethods{
	public RemoteWebDriver driver;
	public int i = 1;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver  = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver  = new FirefoxDriver();
			}

			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} catch (WebDriverException e) {
			System.err.println("WebDriverException has occurred");
			throw new RuntimeException("WebDriverException has occurred");
		}catch (IllegalStateException e) {
			System.err.println("IllegalStateException has occurred");
			throw new RuntimeException("IllegalStateException has occurred");
		}
		catch (NullPointerException e) {
			System.err.println("NullPointerException has occurred");
			throw new RuntimeException("NullPointerException has occurred");
		}
		catch (Exception e) {
			System.err.println("Exception has occurred");
			throw new RuntimeException("Exception has occurred");
		}finally {
			takeSnap();
		}
		System.out.println("The Browser "+browser+" launched successfully");


	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "linktext": return driver.findElementByLinkText(locValue);	
			case "name": return driver.findElementByName(locValue);
			case "cssSelector": return (WebElement) driver.findElementsByCssSelector(locValue);

			}
		} catch (NoSuchElementException e) {
			System.err.println("NoSuchElementException has occurred");
			throw new RuntimeException("NoSuchElementException has occurred");
		}catch (StaleElementReferenceException e) {
			System.err.println("StaleElementReferenceException has occurred");
		}catch (WebDriverException e) {
			System.err.println("WebDriverException has occurred");
			throw new RuntimeException("WebDriverException has occurred");
		}catch (Exception e) {
			System.err.println("Exception has occurred");
			throw new RuntimeException("Exception has occurred");
		}
		return null;
	}
	
	
	public List<WebElement> locateElements(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementsById(locValue);
			case "class": return driver.findElementsByClassName(locValue);
			case "xpath": return driver.findElementsByXPath(locValue);
			case "linktext": return driver.findElementsByLinkText(locValue);	
			case "name": return driver.findElementsByName(locValue);
			case "cssSelector": return driver.findElementsByCssSelector(locValue);

			}
		} catch (NoSuchElementException e) {
			System.err.println("NoSuchElementException has occurred");
			throw new RuntimeException("NoSuchElementException has occurred");
		}catch (StaleElementReferenceException e) {
			System.err.println("StaleElementReferenceException has occurred");
		}catch (WebDriverException e) {
			System.err.println("WebDriverException has occurred");
			throw new RuntimeException("WebDriverException has occurred");
		}catch (Exception e) {
			System.err.println("Exception has occurred");
			throw new RuntimeException("Exception has occurred");
		}
		return null;
	}


	public WebElement locateElement(String locValue) {	
		
	
		return driver.findElementById(locValue);
		
		/*catch (NoSuchElementException e) {
			System.err.println("NoSuchElementException has occurred");
			throw new RuntimeException("NoSuchElementException has occurred");
		}catch (StaleElementReferenceException e) {
			System.err.println("StaleElementReferenceException has occurred");
		}catch (WebDriverException e) {
			System.err.println("WebDriverException has occurred");
			throw new RuntimeException("WebDriverException has occurred");
		}catch (Exception e) {
			System.err.println("Exception has occurred");
			throw new RuntimeException("Exception has occurred");
		}*/
	}

	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
		} catch (NoSuchElementException e) {
			System.err.println("NoSuchElementException has occurred");
			throw new RuntimeException("NoSuchElementException has occurred");
		}catch (StaleElementReferenceException e) {
			System.err.println("StaleElementReferenceException has occurred");
			throw new RuntimeException("StaleElementReferenceException has occurred");
		}catch (WebDriverException e) {
			System.err.println("WebDriverException has occurred");
			throw new RuntimeException("WebDriverException has occurred");
		}catch (Exception e) {
			System.err.println("Exception has occurred");
			throw new RuntimeException("Exception has occurred");
		}finally {
			takeSnap();
		}
		System.out.println("The Data "+data+" is entered Successfully");

	}


	public void click(WebElement ele) {
		try {
			ele.click();
		} catch (NoSuchElementException e) {
			System.err.println("NoSuchElementException has occurred");
			throw new RuntimeException("NoSuchElementException has occurred");
		}catch (StaleElementReferenceException e) {
			System.err.println("StaleElementReferenceException has occurred");
			throw new RuntimeException("StaleElementReferenceException has occurred");
		}catch (WebDriverException e) {
			System.err.println("WebDriverException has occurred");
			throw new RuntimeException("WebDriverException has occurred");
		}catch (Exception e) {
			System.err.println("Exception has occurred");
			throw new RuntimeException("Exception has occurred");
		}
		finally {
			takeSnap();
		}
		
		System.out.println("The Element "+ele+" is clicked Successfully");

	}


	public String getText(WebElement ele) {
		try {
			String text = ele.getText();
		}
		catch (NoSuchElementException e) {
			System.err.println("NoSuchElementException has occurred");
			throw new RuntimeException("NoSuchElementException has occurred");
		}catch (StaleElementReferenceException e) {
			System.err.println("StaleElementReferenceException has occurred");
			throw new RuntimeException("StaleElementReferenceException has occurred");
		}catch (WebDriverException e) {
			System.err.println("WebDriverException has occurred");
			throw new RuntimeException("WebDriverException has occurred");
		}catch (Exception e) {
			System.err.println("Exception has occurred");
			throw new RuntimeException("Exception has occurred");
		}
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value, String valueType) {
		// TODO Auto-generated method stub
		Select dd=new Select(ele);
		if(valueType.equalsIgnoreCase("visibletext")) {

			dd.selectByVisibleText(value);
			takeSnap();
			System.out.println("the value"+value+"has been selected from the picklist");
		}else if(valueType.equalsIgnoreCase("Value")) {

			dd.selectByValue(value);
		}else if(valueType.equalsIgnoreCase("index")) {

			dd.selectByIndex(Integer.parseInt(value));
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		try {
			Select ddIndex=new Select(ele);
			ddIndex.selectByIndex(index);
			takeSnap();
			System.err.println("the text value has been selected based on the index whch is"+index);
		} catch (NoSuchElementException e) {
			System.err.println("NoSuchElementException has been thrown");
			throw new RuntimeException("NoSuchElementException has been thrown");
		}
		catch (StaleElementReferenceException e) {
			System.err.println("StaleElementReferenceException has been thrown");
			throw new RuntimeException("StaleElementReferenceException has been thrown");
		}
		catch (Exception e) {
			System.err.println("Exception has boccurred");
			throw new RuntimeException("Exception has boccurred");
		}
	} 




	@Override
	public boolean verifyTitle(String expectedTitle) {
		// TODO Auto-generated method stub
		boolean btitle=false;
		String title = driver.getTitle();
		if(title.equals(expectedTitle)) {
			btitle=true;
		}
		return btitle;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		

		String text = ele.getText();
		if(text.equals(expectedText)) {
			System.out.println(true);
		}else {
			System.out.println(false);
		}takeSnap();

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if(text.contains(expectedText)) {
			System.out.println("the text matched with the protion of the exoected text ");
		}

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {

		try {
			String attrvalue = ele.getAttribute(value);
			if(attrvalue.equals(value)) {
				System.out.println("value of the given attribute "+attribute+" is "+value);
			}
		} catch (NoSuchElementException e) {
			System.err.println("NoSuchElementException has occurred");
			throw new RuntimeException("NoSuchElementException has occurred");
		}
		catch ( WebDriverException e) {
			System.err.println("webDriverException has occurred");
			throw new RuntimeException("webDriverException has occurred");
		}
		catch (Exception e) {
			System.err.println("Exception has occurred");
			throw new RuntimeException("webDriverException has occurred");
		}
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try {
			String attrvalue = ele.getAttribute(value);
			if(attrvalue.contains(value)) {
				System.out.println("value of the given attribute "+attribute+" is "+value);
			}
		} catch (NoSuchElementException e) {
			System.err.println("NoSuchElementException has occurred");
			throw new RuntimeException("NoSuchElementException has occurred");
		}
		catch ( WebDriverException e) {
			System.err.println("webDriverException has occurred");
			throw new RuntimeException("webDriverException has occurred");
		}
		catch (Exception e) {
			System.err.println("Exception has occurred");
			throw new RuntimeException("webDriverException has occurred");
		}
	}

	

	@Override
	public void verifySelected(WebElement ele) {
		try {
			boolean selected = ele.isSelected();
			System.out.println(selected);
		} catch (NoSuchElementException e) {
			System.err.println("NoSuchElementException has occurred");
			throw new RuntimeException("NoSuchElementException has occurred");
		}
		catch ( WebDriverException e) {
			System.err.println("webDriverException has occurred");
			throw new RuntimeException("webDriverException has occurred");
		}
		catch (Exception e) {
			System.err.println("Exception has occurred");
			throw new RuntimeException("webDriverException has occurred");
		}finally {
			takeSnap();
		}

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		try {
		boolean displayed = ele.isDisplayed();
		System.out.println(displayed);
	} catch (NoSuchElementException e) {
		System.err.println("NoSuchElementException has occurred");
		throw new RuntimeException("NoSuchElementException has occurred");
	}
	catch ( WebDriverException e) {
		System.err.println("webDriverException has occurred");
		throw new RuntimeException("webDriverException has occurred");
	}
	catch (Exception e) {
		System.err.println("Exception has occurred");
		throw new RuntimeException("webDriverException has occurred");
	}finally {
		takeSnap();
	}


	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> windowHandles = driver.getWindowHandles();
			List<String> browserIds=new ArrayList<String>();
			browserIds.addAll(windowHandles);
			driver.switchTo().window(browserIds.get(index));
			takeSnap();
		} catch (NoSuchWindowException e) {
			System.err.println("NoSuchWindowException exceptions has occurred");
		}
		catch (Exception e) {
			System.err.println("Exception exceptions has occurred");
		}

	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
		driver.switchTo().frame(ele);
		}catch(NoSuchFrameException e){
			System.err.println("NoSuchFrameException exception has occurred");
			throw new RuntimeException("NoSuchFrameException exception has occurred");
		}


	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			System.out.println("alert prompt has been attended");
		} catch (NoAlertPresentException e) {
			System.err.println("NoAlertPresentException ha occurred");
			throw new RuntimeException("NoAlertPresentException ha occurred");
		}
		catch (UnhandledAlertException e) {
			System.err.println("UnhandledAlertException ha occurred");
			throw new RuntimeException("UnhandledAlertException ha occurred");
		}

	}

	@Override
	public void dismissAlert() {
		try {
		driver.switchTo().alert().dismiss();
		System.out.println("alert prompt has been dismissed");
	} catch (NoAlertPresentException e) {
		System.err.println("NoAlertPresentException ha occurred");
		throw new RuntimeException("NoAlertPresentException ha occurred");
	}
	catch (UnhandledAlertException e) {
		System.err.println("UnhandledAlertException ha occurred");
		throw new RuntimeException("UnhandledAlertException ha occurred");
	}

	}

	@Override
	public String getAlertText() {
		try {
			String text = driver.switchTo().alert().getText();
			return text;
		} catch (NoAlertPresentException e) {
			System.err.println("NoAlertPresentException ha occurred");
			throw new RuntimeException("NoAlertPresentException ha occurred");
		}
		catch (UnhandledAlertException e) {
			System.err.println("UnhandledAlertException ha occurred");
			throw new RuntimeException("UnhandledAlertException ha occurred");
		}
	}


	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File desc = new File("./snaps/img"+i+".png");		
			FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		try {
			driver.close();
			System.out.println("driver has been closed");
		} catch (WebDriverException e) {
			System.err.println("webdriver exception has occurred");
			throw new RuntimeException("webdriver exception has occurred");
		}

	}

	@Override
	public void closeAllBrowsers() {
		try {
			driver.quit();
			System.out.println("driver has been closed");
		} catch (WebDriverException e) {
			System.err.println("webdriver exception has occurred");
			throw new RuntimeException("webdriver exception has occurred");
		}

	}

}
