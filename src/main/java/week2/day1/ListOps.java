package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListOps {

	public static void main(String[] args) {

		List<String> l=new ArrayList<String>();
		l.add("Nokia");
		l.add("Samsung");
		l.add("moto");
		l.add("Ipone");
		l.add("Oneplus");
		for (String myphonnes : l) {
			System.out.println(myphonnes);
		}
		//	System.out.println("list values before sorting:/n"+l);
		int capacity=l.size();
		System.out.println("count of mobiles: "+capacity);
		System.out.println("the name of the last but one mobile is:"+ l.get(capacity-2));
		System.out.println("the list values after sorted:");
		Collections.sort(l);
		System.out.println(l);
		Collections.reverse(l);



	}

}
